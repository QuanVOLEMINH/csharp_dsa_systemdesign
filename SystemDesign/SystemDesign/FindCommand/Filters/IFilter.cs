using File = SystemDesign.FindCommand.Models.File;

namespace SystemDesign.FindCommand.Filters
{
    public interface IFilter
    {
        bool Apply(File file);
    }
}