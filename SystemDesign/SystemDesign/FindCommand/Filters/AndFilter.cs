using File = SystemDesign.FindCommand.Models.File;

namespace SystemDesign.FindCommand.Filters
{
    public class AndFilter : IFilter
    {

        private readonly IFilter[] _filters;

        public AndFilter(params IFilter[] filters)
        {
            _filters = filters;
        }

        public bool Apply(File file)
        {
            if (_filters == null || _filters.Length == 0)
            {
                return true;
            }

            foreach (var filter in _filters)
            {
                if (!filter.Apply(file))
                {
                    return false;
                }
            }

            return true;
        }
    }
}