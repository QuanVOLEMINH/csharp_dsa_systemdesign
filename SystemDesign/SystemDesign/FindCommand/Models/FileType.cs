namespace SystemDesign.FindCommand.Models
{
    public enum FileType
    {
        Unknown = 0,
        Json = 1,
        Txt = 2,
        Xml = 3,
        Pdf = 4
    }
}