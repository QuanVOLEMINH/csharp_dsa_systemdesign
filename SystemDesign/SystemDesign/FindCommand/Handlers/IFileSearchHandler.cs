using File = SystemDesign.FindCommand.Models.File;

namespace SystemDesign.FindCommand
{
    public interface IFileSearchHandler
    {
        void OnFound(File? file);
    }
}