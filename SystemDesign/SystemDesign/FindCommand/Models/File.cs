namespace SystemDesign.FindCommand.Models
{
    public class File
    {
        public string? Name { get; set; }
        public int Size { get; set; }
        public FileType Type { get; set; }
        public bool IsDirectory { get; set; }
        public File[]? Children { get; set; }
    }
}