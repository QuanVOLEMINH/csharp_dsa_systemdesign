using System;
using System.Collections.Concurrent;
using System.Linq;
using SystemDesign.FindCommand;
using SystemDesign.FindCommand.Models;

namespace SystemDesign.Tests.FindCommand
{
    public class LargeOutputHandler : IFileSearchHandler
    {
        public int BufferSize { get; private set; }
        public ConcurrentQueue<File> Queue { get; private set; }

        public LargeOutputHandler(int bufferSize)
        {
            BufferSize = bufferSize;
            Queue = new ConcurrentQueue<File>();
        }

        public void OnFound(File? file)
        {
            if (file == null) // end process
            {
                DoWork();
                Queue.Clear();
                return;
            }

            if (Queue.Count < BufferSize)
            {
                Queue.Enqueue(file);
                return;
            }

            DoWork();
            Queue.Clear();
            Queue.Enqueue(file);
        }

        private void DoWork()
        {
            Console.WriteLine(string.Join(',', Queue.Select(f => f.Name)));
        }
    }
}