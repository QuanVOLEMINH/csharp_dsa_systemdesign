using System;
using System.Collections.Concurrent;
using System.Linq;
using SystemDesign.FindCommand;
using SystemDesign.FindCommand.Filters;
using SystemDesign.FindCommand.Models;
using Xunit;
using File = SystemDesign.FindCommand.Models.File;

namespace SystemDesign.Tests.FindCommand
{
    public class FindCommandTest
    {
        private readonly File _directory;

        public FindCommandTest()
        {
            _directory = new File
            {
                Name = "system",
                IsDirectory = true,
                Children = new File[]
                {
                    new File{
                        Name = "config",
                        IsDirectory = true,
                        Children = new File[]{
                            new File{
                                Name = "maven_config.xml",
                                IsDirectory = false,
                                Type = FileType.Xml,
                                Size = 6
                            },
                            new File{
                                Name = "apache_config.xml",
                                IsDirectory = false,
                                Type = FileType.Xml,
                                Size = 4
                            },
                            new File{
                                Name = "dotnet_config.json",
                                IsDirectory = false,
                                Type = FileType.Json,
                                Size = 6
                            }
                        }
                    },
                    new File{
                    Name = "lib",
                        IsDirectory = true,
                        Children = new File[]{
                            new File{
                                Name = "access",
                                IsDirectory = true,
                                Children = new File[]{
                                    new File{
                                        Name = "token.json",
                                        IsDirectory = false,
                                        Type = FileType.Json,
                                        Size = 3
                                    }
                                }
                            },
                            new File{
                                Name = "variables.json",
                                IsDirectory = false,
                                Type = FileType.Json,
                                Size = 10
                            }
                        }
                    }
                }
            };
        }

        [Fact]
        public void GivenTypeFilterWhenGetFileWithFiltersToAQueueThenOk()
        {
            var findCommand = new FindService();

            var xmlTypeFilter = new TypeFilter(FileType.Xml);
            var xmlFiles = new ConcurrentQueue<File>();

            findCommand.FindWithFilters(_directory, xmlTypeFilter, xmlFiles);

            Assert.Equal(2, xmlFiles.Count);
            Assert.Contains("maven_config.xml", xmlFiles.Select(f => f.Name));

            var jsonTypeFilter = new TypeFilter(FileType.Json);
            var jsonFiles = new ConcurrentQueue<File>();

            findCommand.FindWithFilters(_directory, jsonTypeFilter, jsonFiles);

            Assert.Equal(3, jsonFiles.Count);
        }

        [Fact]
        public void GivenSizeFilterWhenGetFileWithFiltersToAQueueThenOk()
        {
            var findCommand = new FindService();

            var smallerThanFive = new SizeFilter(5, Operator.SmallerThan);
            var smallerThanFiveFiles = new ConcurrentQueue<File>();

            findCommand.FindWithFilters(_directory, smallerThanFive, smallerThanFiveFiles);

            Assert.Equal(2, smallerThanFiveFiles.Count);
        }

        [Fact]
        public void GivenAndFilterWhenGetFileWithFiltersToAQueueThenOk()
        {
            var findCommand = new FindService();

            var smallerThanFive = new SizeFilter(5, Operator.SmallerThan);
            var xmlTypeFilter = new TypeFilter(FileType.Xml);
            var andFilter = new AndFilter(smallerThanFive, xmlTypeFilter);
            var xmlSmallerThanFiveFiles = new ConcurrentQueue<File>();

            findCommand.FindWithFilters(_directory, andFilter, xmlSmallerThanFiveFiles);

            Assert.Single(xmlSmallerThanFiveFiles);
        }

        [Fact]
        public void GivenLargeOutputWhenGetFileWithFiltersThenHandlerSmallChunkEachTime()
        {
            var findCommand = new FindService();
            var handler = new LargeOutputHandler(2);

            findCommand.FindWithFilters(_directory, handler: handler);

            Assert.Empty(handler.Queue);
        }
    }
}