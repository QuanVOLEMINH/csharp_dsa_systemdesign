using SystemDesign.FindCommand.Models;
using File = SystemDesign.FindCommand.Models.File;

namespace SystemDesign.FindCommand.Filters
{
    public sealed class SizeFilter : IFilter
    {
        private readonly int _size;
        private readonly Operator _op;

        public SizeFilter(int size, Operator op)
        {
            _size = size;
            _op = op;
        }

        public bool Apply(File file)
        {
            switch (_op)
            {
                case Operator.Equal:
                    return file.Size == _size;
                case Operator.LargerThan:
                    return file.Size > _size;
                case Operator.SmallerThan:
                    return file.Size < _size;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}