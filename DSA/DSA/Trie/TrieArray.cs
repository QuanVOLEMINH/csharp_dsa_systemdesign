using System.Text;

namespace DSA
{
    public class TrieArray
    {
        private readonly TrieArrayNode _root;

        public TrieArray()
        {
            _root = new TrieArrayNode();
        }

        public void Insert(string word)
        {
            var node = _root;

            foreach (var c in word.ToCharArray())
            {
                if (!node.ContainsKey(c))
                {
                    node.Put(c, new TrieArrayNode());
                }
                node = node.Get(c);
            }
            node.IsEnd = true;
        }

        public bool Search(string word)
        {
            var node = _root;

            foreach (var c in word.ToCharArray())
            {
                if (!node.ContainsKey(c))
                {
                    return false;
                }
                node = node.Get(c);
            }

            return node.IsEnd;
        }

        public bool StartsWith(string prefix)
        {
            var node = _root;

            foreach (var c in prefix.ToCharArray())
            {
                if (!node.ContainsKey(c))
                {
                    return false;
                }
                node = node.Get(c);
            }

            return true;
        }

        public string SearchLongestPrefix(string word)
        {
            var node = _root;
            var prefix = new StringBuilder();
            foreach (var c in word.ToCharArray())
            {
                if (node.ContainsKey(c) && node.Size == 1 && !node.IsEnd)
                {
                    prefix.Append(c);
                    node = node.Get(c);
                }
                else
                {
                    return prefix.ToString();
                }
            }
            return prefix.ToString();
        }
    }

    public class TrieArrayNode
    {
        public TrieArrayNode[] Children { get; private set; }
        public bool IsEnd { get; set; }
        private const int NbChars = 26;

        // Nb of non null children
        public int Size { get; private set; }

        public TrieArrayNode()
        {
            Children = new TrieArrayNode[NbChars];
            IsEnd = false;
        }

        public bool ContainsKey(char c)
        {
            return Children[c - 'a'] != null;
        }

        public TrieArrayNode Get(char c)
        {
            return Children[c - 'a'];
        }

        public void Put(char c, TrieArrayNode node)
        {
            Children[c - 'a'] = node;
            Size++;
        }
    }
}

