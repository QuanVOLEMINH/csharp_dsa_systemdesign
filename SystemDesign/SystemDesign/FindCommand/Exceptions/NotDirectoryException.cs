namespace SystemDesign.FindCommand.Exceptions
{
    public sealed class NotDirectoryException : Exception
    {
        public NotDirectoryException(string message) : base(message) { }
    }
}