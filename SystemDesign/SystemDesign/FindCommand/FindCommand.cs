using System.Collections.Concurrent;
using SystemDesign.FindCommand.Exceptions;
using SystemDesign.FindCommand.Filters;
using File = SystemDesign.FindCommand.Models.File;

namespace SystemDesign.FindCommand
{
    public class FindService
    {
        public void FindWithFilters(File directory, IFilter? filter, ConcurrentQueue<File> queue)
        {
            if (directory == null) throw new ArgumentNullException(nameof(directory));

            if (!directory.IsDirectory) throw new NotDirectoryException("this is not a directory");

            InternalFindWithFilters(directory, filter, queue);
        }

        public void FindWithFilters(File directory, IFilter? filter = null, IFileSearchHandler? handler = null)
        {
            if (directory == null) throw new ArgumentNullException(nameof(directory));

            if (!directory.IsDirectory) throw new NotDirectoryException("this is not a directory");

            InternalFindWithFilters(directory, filter, handler);
            handler?.OnFound((File?)null);
        }

        private void InternalFindWithFilters(File directory, IFilter? filter, ConcurrentQueue<File> queue)
        {
            if (directory.Children == null) return;

            foreach (var file in directory.Children)
            {
                if (file.IsDirectory)
                {
                    InternalFindWithFilters(file, filter, queue);
                    continue;
                }

                if (filter == null || filter.Apply(file))
                {
                    queue.Enqueue(file);
                }
            }
        }

        private void InternalFindWithFilters(File directory, IFilter? filter, IFileSearchHandler? handler)
        {
            if (directory.Children == null) return;

            foreach (var file in directory.Children)
            {
                if (file.IsDirectory)
                {
                    InternalFindWithFilters(file, filter, handler);
                    continue;
                }

                if (filter == null || filter.Apply(file))
                {
                    handler?.OnFound(file);
                }
            }
        }
    }
}