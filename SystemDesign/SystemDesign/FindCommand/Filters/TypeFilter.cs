using SystemDesign.FindCommand.Models;
using File = SystemDesign.FindCommand.Models.File;

namespace SystemDesign.FindCommand.Filters
{
    public sealed class TypeFilter : IFilter
    {
        private readonly FileType _type;
        public TypeFilter(FileType type)
        {
            _type = type;
        }
        public bool Apply(File file)
        {
            return file?.Type == _type;
        }
    }
}