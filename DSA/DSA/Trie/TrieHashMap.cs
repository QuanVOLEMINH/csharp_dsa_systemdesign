﻿using System.Collections.Generic;

namespace DSA
{
    public class TrieHashMap
    {
        private readonly TrieHashMapNode _root;
        public TrieHashMap()
        {
            _root = new TrieHashMapNode();
        }

        public void Insert(string word)
        {
            var node = _root;

            foreach (var c in word.ToCharArray())
            {
                if (!node.Children.ContainsKey(c))
                {
                    node.Children[c] = new TrieHashMapNode();
                }
                node = node.Children[c];
            }
            node.IsEnd = true;
        }

        public bool Search(string word)
        {
            var node = _root;

            foreach (var c in word.ToCharArray())
            {
                if (!node.Children.ContainsKey(c))
                {
                    return false;
                }
                node = node.Children[c];
            }

            return node.IsEnd;
        }

        public bool StartsWith(string prefix)
        {
            var node = _root;

            foreach (var c in prefix.ToCharArray())
            {
                if (!node.Children.ContainsKey(c))
                {
                    return false;
                }
                node = node.Children[c];
            }

            return true;
        }
    }

    public class TrieHashMapNode
    {
        public IDictionary<char, TrieHashMapNode> Children { get; private set; }
        public bool IsEnd { get; set; }

        public TrieHashMapNode()
        {
            Children = new Dictionary<char, TrieHashMapNode>();
            IsEnd = false;
        }
    }
}

