﻿using System.Collections.Generic;
using Xunit;

namespace DSA.Tests;

public class TrieArrayTest
{
    [Fact]
    public void WhenSeachThenOk()
    {
        var trie = new TrieArray();
        var words = new List<string>
        {
            "am", "being", "bed", "so"
        };

        foreach (var w in words)
        {
            trie.Insert(w);
        }

        Assert.True(trie.Search("bed"));
        Assert.False(trie.Search("bein"));
        Assert.True(trie.Search("am"));
    }

    [Fact]
    public void WhenSeachPrefixThenOk()
    {
        var trie = new TrieArray();
        var words = new List<string>
        {
            "am", "being", "bed", "so"
        };

        foreach (var w in words)
        {
            trie.Insert(w);
        }

        Assert.True(trie.StartsWith("be"));
        Assert.False(trie.StartsWith("bey"));
        Assert.True(trie.StartsWith("so"));
    }

    [Fact]
    public void WhenFindLongestPrefixOfPrefixThenOk()
    {
        var trie = new TrieArray();
        var words = new List<string>
        {
            "flower","flow","flour"
        };

        foreach (var w in words)
        {
            trie.Insert(w);
        }

        Assert.Equal("flo", trie.SearchLongestPrefix(words[0]));
    }
}