namespace SystemDesign.FindCommand.Models
{
    public enum Operator
    {
        None = 0,
        SmallerThan = 1,
        Equal = 2,
        LargerThan = 3
    }
}